<?php

namespace App\Http\Controllers;

use App\Models\AttendanceManager;
use App\Models\AttendanceFilename;
use App\Repositories\ExportRepository;
use App\Repositories\ImportAttendanceData;
use App\Repositories\UploadRepository;

use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;


class AttendanceController extends Controller
{
    public $export;
    public $upload;
    public $attendanceData;

    /**
     * AttendanceController constructor.
     * @param ExportRepository $exportRepository
     * @param UploadRepository $uploadRepository
     * @param ImportAttendanceData $attendanceData
     */
    public function __construct(ExportRepository $exportRepository, UploadRepository $uploadRepository, ImportAttendanceData $attendanceData)
    {
        $this->export = $exportRepository;
        $this->upload = $uploadRepository;
        $this->attendanceData = $attendanceData;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function importAttendanceFile()
    {
        $files = AttendanceFilename::paginate(10);
        return view('hrms.attendance.upload_file', compact('files'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function uploadFile(Request $request)
    {
        if (Input::hasFile('upload_file')) {
            $file = Input::file('upload_file');
            $filename = $this->upload->File($file, $request->description, $request->date);

            try {
                if($filename) {
                    $this->attendanceData->Import($filename);
                }
            } catch(\Exception $e) {

                \Session::flash('flash_message1', $e->getMessage());

                \Log::info($e->getLine(). ' '. $e->getFile());
                return redirect()->back();
            }
        }
        else {

            return redirect()->back()->with('flash_message', 'Please choose a file to upload');
        }



        \Session::flash('flash_message1', 'File successfully Uploaded!');
        return redirect()->back();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showSheetDetails()
    {
        $column = '';
        $string = '';
        $dateFrom = '';
        $dateTo = '';
        $attendances = AttendanceManager::paginate(20);
        return view('hrms.attendance.show_attendance_sheet_details', compact('attendances', 'column', 'string', 'dateFrom', 'dateTo'));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function doDelete($id)
    {
        $file = AttendanceFilename::find($id);
        $file->delete();

        \Session::flash('flash_message1', 'File successfully Deleted!');
        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function searchAttendance(Request $request)
    {
        try {
            $string = $request->string;
            $column = $request->column;

            $dateTo = date_format(date_create($request->dateTo), 'd-m-Y');
            $dateFrom = date_format(date_create($request->dateFrom), 'd-m-Y');

            if ($request->button == 'Search') {
                /**
                 * send the post data to getFilteredSearchResults function
                 * of AttendanceManager class in Models folder
                 */
                $attendances = AttendanceManager::getFilterdSearchResults($request->all());
                return view('hrms.attendance.show_attendance_sheet_details', compact('attendances', 'column', 'string', 'dateFrom', 'dateTo'));
            }
            else
            {
                if ($column && $string) {
                    if ($column == 'status') {
                        $string = convertAttendanceTo($string);
                    }
                    $attendances = AttendanceManager::whereRaw($column . " like '%" . $string . "%'")->get();
                } else {
                    $attendances = AttendanceManager::get();
                }

                $fileName = 'Attendance_Listing_' . rand(1, 1000). '.csv';
                $filePath = storage_path('export/').$fileName;
                $file = new \SplFileObject($filePath, "a");
                $headers = ['id', 'name', 'code', 'date', 'day', 'in_time', 'out_time', 'hours_worked', 'difference', 'status', 'leave_status', 'user_id', 'created_at', 'updated_at'];
                $file->fputcsv($headers);
                foreach($attendances as $attendance)
                {
                    $file->fputcsv([$attendance->id,$attendance->name,$attendance->code,$attendance->date,$attendance->day,$attendance->in_time,$attendance->out_time,$attendance->hours_worked,$attendance->difference,$attendance->status,$attendance->leave_status]);
                }



                /**
                 * sending the results fetched in above query to exportData
                 * function of ExportRepository class located in
                 * app\Repositories folder
                 */

                // $fileName = $this->export->exportData($attendances, $file, $headers);

                return response()->download(storage_path('export/') . $fileName);
            }

        } catch (\Exception $e) {
            return redirect()->back()->with('message', $e->getMessage());
        }
    }


    /**
     * Function to take employee attendance i.e. clock in and out time for that date
     */
    public function myAttendance()
    {
        //Get the authenticated user
        $user = Auth::user();
        //Get today's date
        $today_date = Carbon::now()->toDateString();
        $attendances = AttendanceManager::where('user_id', $user->id)->orderBy('date', 'DESC')->paginate(10);
        /**
         * Check if user has any attendance today.
         * If no send clock in and out time false
         * If yes check clock in and out time and send response
         * according to it.
         */
        $today_attendance = AttendanceManager::where('user_id', $user->id)->where('date', $today_date)->first();
        $clock_in = false;
        $clock_out = false;
        if ($today_attendance && $today_attendance->in_time){
            $clock_in = true;
        }
        if ($today_attendance && $today_attendance->out_time){
            $clock_out = true;
        }
        return view('hrms.attendance.my_attendance',[
            'clock_in' => $clock_in,
            'clock_out' => $clock_out,
            'attendances' => $attendances
        ]);
    }

    public function clockIn()
    {
        //Get the authenticated user
        $user = Auth::user();
        //Get today's date
        $today_date = Carbon::now()->toDateString();
        $in_time = Carbon::now()->toTimeString();
        try{
            $attendance = AttendanceManager::where('user_id', $user->id)->where('date', $today_date)->first();
            if ($attendance && $attendance->in_time){
                return redirect()->back()->with('flash_message', 'You have already clock in today');
            } else{
                $attendance = new AttendanceManager();
                $attendance->user_id = $user->id;
                $attendance->date = $today_date;
                $attendance->in_time = $in_time;
                $attendance->hours_worked = 0;
                $attendance->save();
            }
            return redirect()->back();
        } catch (\Exception $exception) {
            Log::error($exception);
            return redirect()->back()->with('flash_message', $exception->getMessage());
        }

    }

    public function clockOut()
    {
        //Get the authenticated user
        $user = Auth::user();
        //Get today's date
        $today_date = Carbon::now()->toDateString();
        $out_time = Carbon::now()->toTimeString();
        try{
            $attendance = AttendanceManager::where('user_id', $user->id)->where('date', $today_date)->first();

            if ($attendance && !$attendance->in_time){
                return redirect()->back()->with('flash_message', 'You have not clock in today');
            } else{
                $start_time = $attendance->in_time;
                $end_time = $out_time;
                $start_datetime = Carbon::parse($start_time);
                $end_datetime = Carbon::parse($end_time);
                $interval_in_minutes = $end_datetime->diffInMinutes($start_datetime); //$to->diffInHours($from);
                $interval_in_hours = $interval_in_minutes / 60;
                $interval_in_hours = round($interval_in_hours, 2);
                $attendance->out_time = $out_time;
                $attendance->hours_worked = $interval_in_hours;
                $attendance->save();
            }
            return redirect()->back();
        } catch (\Exception $exception) {
            Log::error($exception);
            return redirect()->back()->with('flash_message', $exception->getMessage());
        }

    }

    public function employeeAttendance()
    {
        $attendances = AttendanceManager::orderBy('date', 'DESC')->paginate(10);
        return view('hrms.attendance.employee_attendance',[
            'attendances' => $attendances
        ]);
    }

    public function searchAttendances(Request $request)
    {
        $date_range = $request->datefilter;
        $name = $request->name;
        $attendance = new AttendanceManager();
        if ($name) {
            $attendance->where(function ($query) use ($name) {
                $query->whereHas('user', function ($query) use ($name) {
                    $query->where('name', 'LIKE', '%' . $name . '%');
                });
            });
        }

        if ($date_range){
            $date_array = explode(' - ', $date_range);
            $start_date = $date_array[0];
            $end_date = $date_array[1];
            $attendance->whereBetween('date', [$start_date, $end_date]);
        }
        $attendance->orderBy('date', 'DESC')->paginate(10);
        dd($attendance->toSQL());
        return view('hrms.attendance.employee_attendance',[
            'attendances' => $attendance
        ]);
    }

}






