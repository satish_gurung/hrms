<?php

    namespace App\Http\Controllers;

    use App\EmployeeLeaves;
    use App\Models\AttendanceManager;
    use App\Models\Event;
    use App\Models\LeaveApply;
    use App\Models\LeaveManager;
    use App\Models\Meeting;
    use App\Models\Role;
    use App\User;
    use Carbon\Carbon;
    use Illuminate\Http\Request;
    use Illuminate\Contracts\Mail\Mailer;
    use App\Http\Requests;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Hash;

    class AuthController extends Controller
    {
        public function __construct(Mailer $mailer)
        {
            $this->mailer = $mailer;
        }

        public function showLogin()
        {
            return view('hrms.auth.login');
        }

        public function doLogin(Request $request)
        {
            $email    = $request->email;
            $password = $request->password;

            $user = User::where('email', $email)->first();
            if ($user) {

                if (\Auth::attempt(['email' => $email, 'password' => $password])) {
                    return redirect()->to('welcome');
                } else {
                    \Session::flash('class', 'alert-danger');
                    \Session::flash('message', 'Email or password does not match!');
                }
            } else {
                \Session::flash('class', 'alert-danger');
                \Session::flash('message', 'User id or password does not match!');
            }

            return redirect()->to('/');
        }

        public function doLogout()
        {
            \Auth::logout();

            return redirect()->to('/');
        }

        public function dashboard()
        {
            $events   = $this->convertToArray(Event::where('date', '>', Carbon::now())->orderBy('date', 'desc')->take(3)->get());
            $meetings = $this->convertToArray(Meeting::where('date', '>', Carbon::now())->orderBy('date', 'desc')->take(3)->get());
            $no_of_employees = User::count();
            $no_of_leaves = EmployeeLeaves::count();
            $no_of_present_employee = AttendanceManager::where('date', Carbon::now()->toDateString())->count();
            $weekStartDate = Carbon::now()->startOfWeek()->format('Y-m-d');
            $weekEndDate = Carbon::now()->endOfWeek()->format('Y-m-d');
            $monthStartDate = Carbon::now()->subWeeks(4)->format('Y-m-d');
            $monthEndDate = Carbon::now()->format('Y-m-d');
            $total_salary = DB::table('attendance_managers')
                ->join('employees', 'employees.id', '=', 'attendance_managers.user_id')
                ->select(DB::raw('sum(attendance_managers.hours_worked*employees.salary) AS total_salary'))
                ->whereBetween('date', [$weekStartDate, $weekEndDate])
                ->first();

            $salaries = DB::table('attendance_managers')
                ->join('employees', 'employees.id', '=', 'attendance_managers.user_id')
                ->select(DB::raw('sum(attendance_managers.hours_worked*employees.salary) AS total_salary'))
                ->whereBetween('date', [$monthStartDate, $monthEndDate])
                ->groupBy(DB::raw('WEEK(date)'))
                ->get();

            $departments = DB::table('employees')
                ->select(DB::raw('department, count(id) AS no_of_department'))
                ->groupBy(DB::raw('department'))
                ->get();


            return view('hrms.dashboard', compact('events', 'meetings', 'no_of_employees', 'no_of_leaves', 'no_of_present_employee', 'total_salary', 'salaries', 'departments'));
        }

        public function welcome()
        {
            return view('hrms.auth.welcome');
        }

        public function notFound()
        {
            return view('hrms.auth.not_found');
        }

        public function showRegister()
        {
            return view('hrms.auth.register');
        }

        public function doRegister(Request $request)
        {
            return view('hrms.auth.register');
        }

        public function calendar()
        {
            return view('hrms.auth.calendar');
        }

        public function changePassword()
        {
            return view('hrms.auth.change');
        }

        public function processPasswordChange(Request $request)
        {
            $password = $request->old;
            $user     = User::where('id', \Auth::user()->id)->first();


            if (Hash::check($password, $user->password)) {
                $user->password = bcrypt($request->new);
                $user->save();
                \Auth::logout();

                return redirect()->to('/')->with('message', 'Password updated! LOGIN again with updated password.');
            } else {
                \Session::flash('flash_message', 'The supplied password does not matches with the one we have in records');

                return redirect()->back();
            }
        }

        public function resetPassword()
        {
            return view('hrms.auth.reset');
        }

        public function processPasswordReset(Request $request)
        {
            $email = $request->email;
            $user  = User::where('email', $email)->first();

            if ($user) {
                $string = strtolower(str_random(6));


                $this->mailer->send('hrms.auth.reset_password', ['user' => $user, 'string' => $string], function ($message) use ($user) {
                    $message->from('no-reply@dipi-ip.com', 'Digital IP Insights');
                    $message->to($user->email, $user->name)->subject('Your new password');
                });

                \DB::table('users')->where('email', $email)->update(['password' => bcrypt($string)]);

                return redirect()->to('/')->with('message', 'Login with your new password received on your email');
            } else {
                return redirect()->to('/')->with('message', 'Your email is not registered');
            }

        }

        public function convertToArray($values)
        {
            $result = [];
            foreach ($values as $key => $value) {
                $result[$key] = $value;
            }

            return $result;
        }

    }
