<?php

namespace App\Http\Controllers;

use App\Jobs\ExportData;
use App\Jobs\SendLoginDetailsToUserJob;
use App\Models\Employee;
use App\Models\EmployeeUpload;
use App\Models\Role;
use App\Models\UserRole;
use App\Promotion;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\DatabaseManager;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use File;

class EmpController extends Controller
{
    private $dbManager;

    public function __construct(DatabaseManager $dbManager)
    {
        $this->dbManager = $dbManager;
    }

    public function addEmployee()
    {
        $roles = Role::get();
        $emp_code = User::max('id');
        $emp_code++;

        return view('hrms.employee.add', compact('roles', 'emp_code'));
    }

    public function processEmployee(Request $request)
    {
        $this->dbManager->beginTransaction();
        try{
            /**
             * Upload Image
             */
            $filename = '';
            $new_filename = '';
            if ($request->file('photo')) {
                $file             = $request->file('photo');
                $fileExt          = $file->getClientOriginalExtension();
                $allowedExtension = ['jpg', 'jpeg', 'png'];

                if (!in_array($fileExt, $allowedExtension)) {
                    return response()->json(['title' => 'Error', 'message' => 'Image Extension not allowed.'], 422);
                }
                $filename = $file->getClientOriginalName();
                $new_filename = uniqid().".".File::extension($filename);
                $path = 'profile_pics/';
                $res = Storage::disk('public')->putFileAs($path,  $request->file('photo'), $new_filename);
                if (!$res){
                    return response()->json(['title' => 'Error', 'message' => 'Error while uploading image.'], 422);
                }
            }
            /**
             * Create a user
             */
            $user           = new User;
            $user->name     = $request->emp_name;
            $user->email    = str_replace(' ', '_', strtolower($request->emp_name)) . '@hrms.com';
            $user->password = bcrypt('123456');
            $user->save();


            /**
             * Attach role to user
             */
            $userRole          = new UserRole();
            $userRole->role_id = $request->role;
            $userRole->user_id = $user->id;
            $userRole->save();

            /**
             * Store employee details
             */
            $emp                       = new Employee;
            $emp->photo                = $new_filename;
            $emp->name                 = $request->emp_name;
            $emp->code                 = $request->emp_code;
            $emp->gender               = $request->gender;
            $emp->date_of_birth        = date_format(date_create($request->dob), 'Y-m-d');
            $emp->date_of_joining      = date_format(date_create($request->doj), 'Y-m-d');
            $emp->number               = $request->number;
            $emp->address              = $request->address;
            $emp->email                = $request->email;
            $emp->department           = $request->department;
            $emp->salary               = $request->salary;
            $emp->tfn_number           = $request->tfn_number;
            $emp->account_number       = $request->account_number;
            $emp->bank_name            = $request->bank_name;
            $emp->bsb                  = $request->bsb;
            $emp->user_id              = $user->id;
            $emp->save();

            $this->dbManager->commit();

            /**
             * Send login details to client
             */
            dispatch(new SendLoginDetailsToUserJob($user, $request->email));

            return response()->json(['title' => 'success', 'message' => 'Employee added successfully'], 200);
        }catch (\Exception $exception){
            $this->dbManager->rollBack();
            Log::error($exception);
            return response()->json(['title' => 'Error', 'message' => 'Error while adding employee.'], 422);
        }

    }

    public function showEmployee()
    {
        $emps   = User::with('employee', 'role.role')->orderBy('id', 'DESC')->paginate(15);
        $column = '';
        $string = '';

        return view('hrms.employee.show_emp', compact('emps', 'column', 'string'));
    }

    public function showEdit($id)
    {
        //$emps = Employee::whereid($id)->with('userrole.role')->first();
        $emps = User::where('id', $id)->with('employee', 'role.role')->first();

        $roles = Role::get();

        return view('hrms.employee.add', compact('emps', 'roles'));
    }

    public function doEdit(Request $request, $id)
    {
        $this->dbManager->beginTransaction();
        try{
            /**
             * Find the $employee that need to be updated and update it
             */
            $emp                       = Employee::where('user_id', $id)->first();
            $emp->name                 = $request->emp_name;
            $emp->code                 = $request->emp_code;
            $emp->gender               = $request->gender;
            $emp->date_of_birth        = date_format(date_create($request->dob), 'Y-m-d');
            $emp->date_of_joining      = date_format(date_create($request->doj), 'Y-m-d');
            $emp->number               = $request->number;
            $emp->address              = $request->address;
            $emp->email                = $request->email;
            $emp->department           = $request->department;
            $emp->salary               = $request->salary;
            $emp->tfn_number           = $request->tfn_number;
            $emp->account_number       = $request->account_number;
            $emp->bank_name            = $request->bank_name;
            $emp->bsb                  = $request->bsb;
            $emp->save();

            /**
             *  update the role if role is changed
             */
            $emp_role = $request->role;
            if (isset($emp_role)) {
                $userRole = UserRole::firstOrNew(['user_id' => $emp->user_id]);
                $userRole->role_id = $emp_role;
                $userRole->save();
            }
            $this->dbManager->commit();
            return response()->json(['title' => 'success', 'message' => 'Employee updated successfully'], 200);
        }catch (\Exception $exception){
            $this->dbManager->rollBack();
            Log::error($exception);
            return response()->json(['title' => 'Error', 'message' => 'Error while updating employee.'], 422);
        }
    }

    public function doDelete($id)
    {

        $emp = User::find($id);
        $emp->delete();

        \Session::flash('flash_message', 'Employee successfully Deleted!');

        return redirect()->back();
    }

    public function importFile()
    {
        return view('hrms.employee.upload');
    }

    public function uploadFile(Request $request)
    {
        $files = Input::file('upload_file');

        $this->dbManager->beginTransaction();
        try{
            foreach ($files as $file) {
                Excel::load($file, function ($reader) {
                    $rows = $reader->get([
                        'emp_name',
                        'role',
                        'gender',
                        'dob',
                        'doj',
                        'email',
                        'mob_number',
                        'address',
                        'department',
                        'salary',
                        'account_number',
                        'bank_name',
                        'bsb',
                        'tfn'
                    ]);
                    $emp_code = User::max('id');
                    $emp_code++;
                    foreach ($rows as $row) {
                        \Log::info($row->role);
                        $user           = new User;
                        $user->name     = $row->emp_name;
                        $user->email    = str_replace(' ', '_', strtolower($row->emp_name)) . '@hrms.com';
                        $user->password = bcrypt('123456');
                        $user->save();
                        $attachment         = new Employee();
                        $attachment->photo  = '';
                        $attachment->name   = $row->emp_name;
                        $attachment->code   = "EMP#".$emp_code;
                        $emp_code++;

                        if (empty($row->gender)) {
                            $attachment->gender = 'Not Exist';
                        } else {
                            $attachment->gender = $row->gender;
                        }
                        if (empty($row->dob)) {
                            $attachment->date_of_birth = '0000-00-00';
                        } else {
                            $attachment->date_of_birth = date('Y-m-d', strtotime($row->dob));
                        }
                        if (empty($row->doj)) {
                            $attachment->date_of_joining = '0000-00-00';
                        } else {
                            $attachment->date_of_joining = date('Y-m-d', strtotime($row->doj));
                        }
                        if (empty($row->mob_number)) {
                            $attachment->number = '1234567890';
                        } else {
                            $attachment->number = $row->mob_number;
                        }
                        if (empty($row->address)) {
                            $attachment->address = 'Not Exist';
                        } else {
                            $attachment->address = $row->address;
                        }
                        if (empty($row->department)) {
                            $attachment->department = 'Not Exist';
                        } else {
                            $attachment->department = $row->department;
                        }
                        if (empty($row->salary)) {
                            $attachment->salary = '00000';
                        } else {
                            $attachment->salary = $row->salary;
                        }
                        if (empty($row->account_number)) {
                            $attachment->account_number = '';
                        } else {
                            $attachment->account_number = $row->account_number;
                        }
                        if (empty($row->bank_name)) {
                            $attachment->bank_name = '';
                        } else {
                            $attachment->bank_name = $row->bank_name;
                        }
                        if (empty($row->bsb)) {
                            $attachment->bsb = '';
                        } else {
                            $attachment->bsb = $row->bsb;
                        }
                        if (empty($row->tfn)) {
                            $attachment->tfn_number = '';
                        } else {
                            $attachment->tfn_number = $row->tfn;
                        }
                        if (empty($row->email)) {
                            $attachment->email = '';
                        } else {
                            $attachment->email = $row->email;
                        }
                        $attachment->user_id = $user->id;
                        $attachment->save();

                        $userRole          = new UserRole();
                        $userRole->role_id = convertRole($row->role);
                        $userRole->user_id = $user->id;
                        $userRole->save();

                    }

                    return 1;
                    //return redirect('upload_form');*/
                }
                );

            }
            $this->dbManager->commit();

            \Session::flash('success', ' Employee details uploaded successfully.');

            return redirect()->back();
        }catch (\Exception $exception){
            $this->dbManager->rollBack();
            Log::error($exception);
            \Session::flash('error', $exception->getMessage());

            return redirect()->back();
        }





    }

    public function searchEmployee(Request $request)
    {
        $string = $request->string;
        $column = $request->column;

        if ($request->button == 'Search') {
            if ($string == '' && $column == '') {
                \Session::flash('success', ' Employee details uploaded successfully.');
                return redirect()->to('employee-manager');
            } elseif ($string != '' && $column == '') {
                \Session::flash('failed', ' Please select category.');
                return redirect()->to('employee-manager');
            }elseif ($column == 'email') {
                $emps = User::with('employee')->where($column,'like', "%$string%")->paginate(20);
            } else {
                $emps = User::with('employee')->whereHas('employee', function ($q) use ($column, $string) {
                    $q->where($column, 'like', "%$string%");
                }
                )->paginate(20);
            }

            return view('hrms.employee.show_emp', compact('emps', 'column', 'string'));
        } else {
            if ($column == '') {
                $emps = User::with('employee')->get();
            } elseif ($column == 'email') {
                $emps = User::with('employee')->where($request->column, $request->string)->paginate(20);
            } else {
                $emps = User::whereHas('employee', function ($q) use ($column, $string) {
                    $q->whereRaw($column . " like '%" . $string . "%'");
                }
                )->with('employee')->get();
            }

            $fileName = 'Employee_Listing_' . rand(1, 1000) . '.csv';
            $filePath = storage_path('export/') . $fileName;
            $file     = new \SplFileObject($filePath, "a");
            // Add header to csv file.
            $headers = ['id', 'photo', 'code', 'name', 'gender', 'date_of_birth', 'date_of_joining', 'number', 'email', 'address', 'department', 'salary', 'account_number', 'bank_name', 'bsb', 'tfn'];
            $file->fputcsv($headers);
            foreach ($emps as $emp) {
                $file->fputcsv([
                        $emp->id,
                        (
                        $emp->employee->photo) ? $emp->employee->photo : 'Not available',
                        $emp->employee->code,
                        $emp->employee->name,
                        $emp->employee->gender,
                        $emp->employee->date_of_birth,
                        $emp->employee->date_of_joining,
                        $emp->employee->number,
                        $emp->employee->email,
                        $emp->employee->address,
                        $emp->employee->department,
                        $emp->employee->salary,
                        $emp->employee->account_number,
                        $emp->employee->bank_name,
                        $emp->employee->bsb,
                        $emp->employee->tfn_number,
                    ]
                );
            }

            return response()->download(storage_path('export/') . $fileName);
        }
    }


    public function showDetails()
    {
        $emps = User::with('employee')->paginate(15);

        return view('hrms.employee.show_bank_detail', compact('emps'));
    }

    public function updateAccountDetail(Request $request)
    {
        try {
            $model                    = Employee::where('id', $request->employee_id)->first();
            $model->bank_name         = $request->bank_name;
            $model->account_number    = $request->account_number;
            $model->ifsc_code         = $request->ifsc_code;
            $model->pf_account_number = $request->pf_account_number;
            $model->save();

            return json_encode('success');
        } catch (\Exception $e) {
            \Log::info($e->getMessage() . ' on ' . $e->getLine() . ' in ' . $e->getFile());

            return json_encode('failed');
        }

    }

    public function doPromotion()
    {
        $emps  = User::get();
        $roles = Role::get();

        return view('hrms.promotion.add_promotion', compact('emps', 'roles'));
    }

    public function getPromotionData(Request $request)
    {
        $result = Employee::with('userrole.role')->where('id', $request->employee_id)->first();
        if ($result) {
            $result = ['salary' => $result->salary, 'designation' => $result->userrole->role->name];
        }

        return json_encode(['status' => 'success', 'data' => $result]);
    }

    public function processPromotion(Request $request)
    {

        $newDesignation  = Role::where('id', $request->new_designation)->first();
        $process         = Employee::where('id', $request->emp_id)->first();
        $process->salary = $request->new_salary;
        $process->save();

        \DB::table('user_roles')->where('user_id', $process->user_id)->update(['role_id' => $request->new_designation]);

        $promotion                    = new Promotion();
        $promotion->emp_id            = $request->emp_id;
        $promotion->old_designation   = $request->old_designation;
        $promotion->new_designation   = $newDesignation->name;
        $promotion->old_salary        = $request->old_salary;
        $promotion->new_salary        = $request->new_salary;
        $promotion->date_of_promotion = date_format(date_create($request->date_of_promotion), 'Y-m-d');
        $promotion->save();

        \Session::flash('flash_message', 'Employee successfully Promoted!');

        return redirect()->back();
    }

    public function showPromotion()
    {
        $promotions = Promotion::with('employee')->paginate(10);

        return view('hrms.promotion.show_promotion', compact('promotions'));
    }

}