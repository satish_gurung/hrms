<?php

namespace App\Http\Controllers;

use App\Models\AttendanceManager;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PayrollController extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {
        $emps   = User::with('employee', 'role.role')->paginate(15);
        $column = '';
        $string = '';

        return view('hrms.payroll.employee_salary', compact('emps', 'column', 'string'));
    }

    public function payslip($id)
    {
        $user = User::with('employee', 'role.role')->where('id', $id)->first();
        $yearStartDate = Carbon::now()->startOfYear()->format('Y-m-d');
        $yearEndDate = Carbon::now()->endOfYear()->format('Y-m-d');
        $weekStartDate = Carbon::now()->startOfWeek()->format('Y-m-d');
        $weekEndDate = Carbon::now()->endOfWeek()->format('Y-m-d');
        $hours_worked = (double) AttendanceManager::where('user_id', $user->id)->whereBetween('date', [$weekStartDate, $weekEndDate])->sum('hours_worked');
        $hours_worked_ytd = (double) AttendanceManager::where('user_id', $user->id)->whereBetween('date', [$yearStartDate, $yearEndDate])->sum('hours_worked');
        $emp_salary = (double) $user->employee->salary;
        $salary_this_week = $hours_worked * $emp_salary;
        $salary_ytd = $hours_worked_ytd * $emp_salary;
        $tax_this_week = 0.07 *  $salary_this_week;
        $tax_ytd = 0.07 * $salary_ytd;
        $super_this_week = 0.05 *  $salary_this_week;
        $super_ytd = 0.05 * $salary_ytd;
        $net_earning = $salary_this_week - $tax_this_week - $super_this_week;

        return view('hrms.payroll.payslip', [
            'user' => $user,
            'hours_worked' => $hours_worked,
            'salary_this_week' => $salary_this_week,
            'salary_ytd' => $salary_ytd,
            'weekStartDate' => $weekStartDate,
            'weekEndDate' => $weekEndDate,
            'tax_this_week' => $tax_this_week,
            'tax_ytd' => $tax_ytd,
            'super_this_week' => $super_this_week,
            'super_ytd' => $super_ytd,
            'net_earning' => $net_earning,
        ]);
    }
}
