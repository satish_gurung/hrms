@extends('hrms.layouts.base')
@section('css')
    @parent()
@endsection

@section('js')
    @parent()
    <script src="{{ asset('assets/js/printThis.js') }}"></script>
    <script>
        function printPayslip() {
            $("#payslip-pdf").printThis({
                importCSS: true,
                debug: true,
                loadCSS: [
                    '{{ asset('assets/custom.css') }}',
                    'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'

                ],
            });
        }
    </script>
@endsection

@section('content')
    <!-- START CONTENT -->
    <div class="content">

        <header id="topbar" class="alt">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="breadcrumb-icon">
                        <a href="/dashboard">
                            <span class="fa fa-home"></span>
                        </a>
                    </li>
                    <li class="breadcrumb-active">
                        <a href="/dashboard"> Dashboard </a>
                    </li>
                    <li class="breadcrumb-link">
                        <a href=""> Employees </a>
                    </li>
                    <li class="breadcrumb-current-item"> Payslip</li>
                </ol>
            </div>
        </header>


        <!-- -------------- Content -------------- -->
        <section id="content" class="table-layout animated fadeIn">

            <!-- -------------- Column Center -------------- -->
            <div class="chute chute-center">

                <!-- -------------- Products Status Table -------------- -->
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-success">
                            <div class="panel">
                                <div class="row">
                                    <div class="col-md-6 panel-heading">
                                        <span class="panel-title hidden-xs">Payslip</span><br />
                                    </div>
                                    <div class="col-md-6">
                                        <button class="btn btn-success pull-right" onclick="printPayslip()">
                                            <span class="fa fa-print"></span> Print
                                        </button>
                                    </div>
                                </div>
                                <br />
                                @if(Session::has('failed'))
                                    <div class="alert alert-danger">
                                        {{ Session::get('failed') }}
                                    </div>
                                @endif

                                <div class="panel-body pn">
                                    @if(Session::has('flash_message'))
                                        <div class="alert alert-success">
                                            {{ Session::get('flash_message') }}
                                        </div>
                                    @endif
                                        <div class="pdf-wrapper" id="payslip-pdf">
                                            <header class="pdf-header">
                                                <div class="pdf-title row">
                                                    <div class="col-md-12">
                                                        <h3 class="text-center">PAYSLIP #123</h3>
                                                    </div>
                                                </div>
                                                <div class="header row">
                                                    <div class="col-md-4 four columns pdf-customer-details text-left">
                                                        <p><b>{{ $user->name }}</b><br/>
                                                            Web designer<br/>
                                                            Employee ID: {{ $user->employee->code }}<br/>
                                                            {{ $user->employee->permanent_address }}
                                                        </p>
                                                    </div>
                                                    <div class="col-md-4 four columns pdf-date text-center">
                                                        <p><b>Payment Date</b></p>
                                                        <p>{{ date('Y-m-d') }}</p>
                                                    </div>
                                                    <div class="col-md-4 four columns pdf-company-details text-right">
                                                        {{--<img src="/images/logo.png" alt="" height={40}/>--}}
                                                        <p>
                                                            HRMS Company<br/>
                                                            8/3 York St Newtown NSW 2100<br/>
                                                            ABN: 12032139023<br/>
                                                            Website: www.hrms.com.au
                                                        </p>
                                                    </div>
                                                </div>
                                            </header>

                                            <section class="pdf-content">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <p class="payslip-title">Earning ({{ $weekStartDate }} to {{ $weekEndDate }})</p>
                                                        <div class="table-responsive">
                                                            <table class="table">
                                                                <thead>
                                                                <tr>
                                                                    <th class="text-left">Description</th>
                                                                    <th class="text-left">Type</th>
                                                                    <th class="text-center">Rate</th>
                                                                    <th class="text-center">Hours</th>
                                                                    <th class="text-center">Amount This pay</th>
                                                                    <th class="text-right">Year Till Date</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <td class="text-left">Ordinary Pay</td>
                                                                    <td class="text-left">Salary</td>
                                                                    <td class="text-center">$ {{ $user->employee->salary }}</td>
                                                                    <td class="text-center">{{ $hours_worked }}</td>
                                                                    <td class="text-center">$ {{ $salary_this_week }}</td>
                                                                    <td class="text-right">$ {{ $salary_ytd }}</td>
                                                                </tr>
                                                                <tr class="table-active">
                                                                    <th class="text-right" colspan="4">Total Payment</th>
                                                                    <th class="text-center">$ {{ $salary_this_week }}</th>
                                                                    <th class="text-right">$ {{ $salary_ytd }}</th>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <p class="payslip-title">Deduction</p>
                                                        <div class="table-responsive">
                                                            <table class="table">
                                                                <thead>
                                                                <tr>
                                                                    <th class="text-left">Description</th>
                                                                    <th class="text-left">Type</th>
                                                                    <th class="text-center">Amount This pay</th>
                                                                    <th class="text-right">Year Till Date</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <td class="text-left">Tax</td>
                                                                    <td class="text-left">PayG</td>
                                                                    <td class="text-center">$ {{ $tax_this_week }}</td>
                                                                    <td class="text-right">$ {{ $tax_ytd }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="text-left">Superannuation</td>
                                                                    <td class="text-left">SGC - AustralianSuper</td>
                                                                    <td class="text-center">$ {{ $super_this_week }}</td>
                                                                    <td class="text-right">$ {{ $super_ytd }}</td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <p class="payslip-title">Payment Details</p>
                                                        <div class="table-responsive">
                                                            <table class="table">
                                                                <thead>
                                                                <tr>
                                                                    <th class="text-left">Account no</th>
                                                                    <th class="text-left">Account Name</th>
                                                                    <th class="text-center">Reference</th>
                                                                    <th class="text-right">Amount</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <td class="text-left">({{ $user->employee->ifsc_code }}) {{ $user->employee->pf_account_number }}</td>
                                                                    <td class="text-left">{{ $user->employee->name }}</td>
                                                                    <td class="text-center">HRMS Company</td>
                                                                    <td class="text-right">$ {{ $net_earning }}</td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>


                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
@endsection