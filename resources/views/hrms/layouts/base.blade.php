<!DOCTYPE html>
<html>

<head>
    <!-- -------------- Meta and Title -------------- -->
    <meta charset="utf-8">
    <title> Human Resource Management System </title>
    <meta name="description" content="HRMS">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf_token" content="{{csrf_token()}}">

@section('css')
        <!-- -------------- Fonts -------------- -->
        <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>
        <link href='https://fonts.googleapis.com/css?family=Lato:400,300,300italic,400italic,700,700italic' rel='stylesheet'
              type='text/css'>


        <!-- -------------- Icomoon -------------- -->
        <link rel="stylesheet" type="text/css" href="/assets/fonts/icomoon/icomoon.css">

        <!-- -------------- CSS - theme -------------- -->
        <link rel="stylesheet" type="text/css" href="/assets/skin/default_skin/css/theme.css">
       <!-- -------------- Favicon -------------- -->
        <link rel="shortcut icon" href="/assets/img/favicon.png">
@show

        <!--  Custom css -->
        <link rel="stylesheet" type="text/css" href="/assets/custom.css">

        <!-- -------------- IE8 HTML5 support  -------------- -->
        <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>

<body class="dashboard-page">


<!-- -------------- Body Wrap  -------------- -->
<div id="main">

    <!-- -------------- Header  -------------- -->
@include('hrms.layouts.header')
<!-- -------------- /Header  -------------- -->

    <!-- -------------- Sidebar  -------------- -->
    <aside id="sidebar_left" class="nano nano-light affix">

        <!-- -------------- Sidebar Left Wrapper  -------------- -->
        <div class="sidebar-left-content nano-content">

            <!-- -------------- Sidebar Header -------------- -->
            <header class="sidebar-header">

                <!-- -------------- Sidebar - Author -------------- -->
            @include('hrms.layouts.sidebar')

            <!-- -------------- Sidebar Hide Button -------------- -->
                <div class="sidebar-toggler">
                    <a href="#">
                        <span class="fa fa-arrow-circle-o-left"></span>
                    </a>
                </div>
                <!-- -------------- /Sidebar Hide Button -------------- -->
            </header>

        </div>
        <!-- -------------- /Sidebar Left Wrapper  -------------- -->

    </aside>

    <!-- -------------- /Sidebar -------------- -->

    <!-- -------------- Main Wrapper -------------- -->
    <section id="content_wrapper">



        <!-- YIELD CONTENT -->

        @yield('content')



    @if(\Route::getFacadeRoot()->current()->uri() == 'dashboard' || \Route::getFacadeRoot()->current()->uri() == 'welcome' || \Route::getFacadeRoot()->current()->uri() == 'change-password' ||
    \Route::getFacadeRoot()->current()->uri() == 'not-found' )
        <!-- -------------- Page Footer -------------- -->
            <footer id="content-footer" class="affix">
                <div class="row">
                    <div class="col-md-6">
                    <span class="footer-legal">{{ date('Y') }} All rights reserved. By <a
                                href="#" target="_blank">Group 4</a></span>
                    </div>
                    <div class="col-md-6 text-right">
                        <span class="footer-meta"></span>
                        <a href="#content" class="footer-return-top">
                            <span class="fa fa-angle-up"></span>
                        </a>
                    </div>
                </div>
            </footer>
            <!-- -------------- /Page Footer -------------- -->
        @endif

    </section>
    <!-- -------------- /Main Wrapper -------------- -->


</div>
<!-- -------------- /Body Wrap  -------------- -->





<!-- -------------- Scripts -------------- -->
@section('js')
    <!-- -------------- jQuery -------------- -->
    <script src="/assets/js/jquery/jquery-1.11.3.min.js"></script>
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>--}}
    <script src="/assets/js/jquery/jquery_ui/jquery-ui.min.js"></script>

    <!-- -------------- Widget JS -------------- -->
    {{--<script src="/assets/js/demo/widgets.js"></script>--}}
    {{--<script src="/assets/js/demo/widgets_sidebar.js"></script>--}}
    <script src="/assets/js/pages/dashboard1.js"></script>

@show

<!-- -------------- Theme Scripts -------------- -->
<script src="/assets/js/utility/utility.js"></script>
<script src="/assets/js/demo/demo.js"></script>
<script src="/assets/js/main.js"></script>

</body>
</html>