@extends('hrms.layouts.base')

@section('content')
    <!-- START CONTENT -->
    <div class="content">

        <header id="topbar" class="alt">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="breadcrumb-icon">
                        <a href="/dashboard">
                            <span class="fa fa-home"></span>
                        </a>
                    </li>
                    <li class="breadcrumb-active">
                        <a href="/dashboard"> Dashboard </a>
                    </li>
                    <li class="breadcrumb-link">
                        <a href=""> Attendance </a>
                    </li>
                    <li class="breadcrumb-current-item"> Attendance Manager</li>
                </ol>
            </div>
        </header>


        <!-- -------------- Content -------------- -->
        <section id="content" class="table-layout animated fadeIn">

            <!-- -------------- Column Center -------------- -->
            <div class="chute chute-center">

                <!-- -------------- Products Status Table -------------- -->
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-success">
                            <div class="panel">
                                <div class="panel-heading">
                                    <span class="panel-title hidden-xs"> My Attendance </span><br />
                                </div><br />

                                <div class="panel-body pn">
                                    @if(Session::has('flash_message'))
                                        <div class="alert alert-success">
                                            {{ Session::get('flash_message') }}
                                        </div>
                                    @endif
                                    @if(!$clock_in && !$clock_out)
                                            <form action="{{route('clock-in')}}" method="post">
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-lg btn-success">
                                                    <span class="fa fa-clock-o"></span>&nbsp;
                                                    CLOCK IN
                                                </button>
                                            </form>
                                    @endif
                                    @if($clock_in && !$clock_out)

                                            <form action="{{route('clock-out')}}" method="post">
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-lg btn-danger">
                                                    <span class="fa fa-clock-o"></span>&nbsp;
                                                    CLOCK OUT
                                                </button>
                                            </form>
                                    @endif
                                    @if($clock_in && $clock_out)
                                        <h3>You have already clock in and out today.</h3>
                                    @endif
                                    <hr/>
                                        <div class="table-responsive">
                                            <table class="table allcp-form theme-warning tc-checkbox-1 fs13">
                                                <thead>
                                                    <tr class="bg-light">
                                                        <th class="text-center">#</th>
                                                        <th class="text-center">Date</th>
                                                        <th class="text-center">In Time</th>
                                                        <th class="text-center">Out Time</th>
                                                        <th class="text-center">Hours Worked</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @if(count($attendances) > 0)
                                                @foreach($attendances as $key => $attendance)
                                                    <tr>
                                                        <td class="text-center">{{$key+1}}</td>
                                                        <td class="text-center">{{getFormattedDate($attendance->date)}}</td>
                                                        <td class="text-center">{{$attendance->in_time}}</td>
                                                        <td class="text-center">{{$attendance->out_time}}</td>
                                                        <td class="text-center">{{$attendance->hours_worked}}</td>
                                                    </tr>
                                                @endforeach
                                                @else
                                                <tr>
                                                    <td colspan="11">
                                                        <p class="text-center">Nothing to show</p>
                                                    </td>
                                                </tr>
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
@endsection
@push('scripts')
    <script src="/assets/js/custom.js"></script>
@endpush