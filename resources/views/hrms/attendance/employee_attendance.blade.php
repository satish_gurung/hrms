@extends('hrms.layouts.base')

@section('css')
    @parent()
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection

@section('js')
    @parent()
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript">
        $(function() {

            $('input[name="datefilter"]').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear'
                }
            });

            $('input[name="datefilter"]').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
            });

            $('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });

        });
    </script>
@endsection
@section('content')
    <!-- START CONTENT -->
    <div class="content">

        <header id="topbar" class="alt">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="breadcrumb-icon">
                        <a href="/dashboard">
                            <span class="fa fa-home"></span>
                        </a>
                    </li>
                    <li class="breadcrumb-active">
                        <a href="/dashboard"> Dashboard </a>
                    </li>
                    <li class="breadcrumb-link">
                        <a href=""> Attendance </a>
                    </li>
                    <li class="breadcrumb-current-item"> Attendance Manager</li>
                </ol>
            </div>
        </header>


        <!-- -------------- Content -------------- -->
        <section id="content" class="table-layout animated fadeIn">

            <!-- -------------- Column Center -------------- -->
            <div class="chute chute-center">

                <!-- -------------- Products Status Table -------------- -->
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-success">
                            <div class="panel" style="padding: 10px;">
                                {{--<div class="panel-heading">--}}
                                    {{--<span class="panel-title hidden-xs"> Employee Attendance </span><br />--}}
                                {{--</div>--}}
                                <div class="panel-body" style="margin-top: 0;">
                                    <form action="{{ route('search-attendance') }}" method="post">
                                        {{ csrf_field() }}
                                        <div class="form-group row">
                                            <div class="col-md-3">
                                                <label for="name">Full name</label>
                                                <input type="text" name="name" id="name" class="form-control">
                                            </div>
                                            <div class="col-md-3">
                                                <label for="datefilter">Date</label>
                                                <input type="text" name="datefilter" id="datefilter" class="form-control">
                                            </div>
                                            <div class="col-md-3">
                                                <br/>
                                                <button type="submit" class="btn btn-success">
                                                    <span class="fa fa-search"></span>&nbsp;Search
                                                </button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="box">
                            <div class="panel">
                                <div class="panel-body pn">
                                    <div class="table-responsive">
                                        <table class="table allcp-form theme-warning tc-checkbox-1 fs13">
                                            <thead>
                                            <tr class="bg-light">
                                                <th class="text-center">#</th>
                                                <th class="text-center">Name</th>
                                                <th class="text-center">Date</th>
                                                <th class="text-center">In Time</th>
                                                <th class="text-center">Out Time</th>
                                                <th class="text-center">Hours Worked</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if(count($attendances) > 0)
                                                @foreach($attendances as $key => $attendance)
                                                    <tr>
                                                        <td class="text-center">{{$key+1}}</td>
                                                        <td class="text-center">{{$attendance->user->name}}</td>
                                                        <td class="text-center">{{getFormattedDate($attendance->date)}}</td>
                                                        <td class="text-center">{{$attendance->in_time}}</td>
                                                        <td class="text-center">{{$attendance->out_time}}</td>
                                                        <td class="text-center">{{$attendance->hours_worked}}</td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="11">
                                                        <p class="text-center">Nothing to show</p>
                                                    </td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="pagination">
                                        {{ $attendances->links() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
@endsection
@push('scripts')
    <script src="/assets/js/custom.js"></script>
@endpush