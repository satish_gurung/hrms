@extends('hrms.layouts.base')

@section('js')
    @parent()
    <script src="{{ asset('assets/js/Chart.min.js') }}"></script>
    <script>

        $(function () {
            let res = {!! json_encode($salaries->toArray()) !!};
            let labels = [];
            let data = [];
            let departments = {!! json_encode($departments->toArray()) !!};
            let dep_labels = [];
            let dep_data = [];
            for (var i = 0; i < res.length; i++) {
                labels.push("Week "+(i+1));
                data.push(res[i].total_salary);
                //Do something
            }
            for (var i = 0; i < departments.length; i++) {
                dep_labels.push(departments[i].department);
                dep_data.push(departments[i].no_of_department);
                //Do something
            }
            console.log(data);

            var lineData = {
                labels: labels,
                datasets: [

                    {
                        label: "Salary",
                        backgroundColor: 'rgba(26,179,148,0.5)',
                        borderColor: "rgba(26,179,148,0.7)",
                        pointBackgroundColor: "rgba(26,179,148,1)",
                        pointBorderColor: "#fff",
                        data: data
                    }
                    // , {
                    //     label: "Data 2",
                    //     backgroundColor: 'rgba(220, 220, 220, 0.5)',
                    //     pointBorderColor: "#fff",
                    //     data: [65, 59, 80, 81, 56, 55, 40]
                    // }
                ]
            };

            var lineOptions = {
                responsive: true
            };
            var ctx = document.getElementById("lineChart").getContext("2d");
            new Chart(ctx, {type: 'line', data: lineData, options:lineOptions});


            var doughnutData = {
                labels: dep_labels,
                datasets: [{
                    data: dep_data,
                    backgroundColor: ["#a3e1d4","#dedede","#b5b8cf"]
                }]
            } ;


            var doughnutOptions = {
                responsive: true
            };


            var ctx4 = document.getElementById("doughnutChart").getContext("2d");
            new Chart(ctx4, {type: 'doughnut', data: doughnutData, options:doughnutOptions});

        });
    </script>
@endsection

@section('content')

        <!-- -------------- Topbar -------------- -->
<header id="topbar" class="alt">
    <div class="topbar-left">
        <ol class="breadcrumb">
            <li class="breadcrumb-icon">
                <a href="/dashboard">
                    <span class="fa fa-home"></span>
                </a>
            </li>
            <li class="breadcrumb-active">
                <a href="/dashboard">Dashboard</a>
            </li>
            <li class="breadcrumb-link">
                <a href="/dashboard">Home</a>
            </li>
            <li class="breadcrumb-current-item">Dashboard</li>
        </ol>
    </div>

</header>
<!-- -------------- /Topbar -------------- -->

<!-- -------------- Content -------------- -->
<section id="content" class="table-layout animated fadeIn">

    <!-- -------------- Column Center -------------- -->
    <div class="chute chute-center">

        <!-- -------------- Quick Links -------------- -->
        <div class="row">
            @if(Auth::user()->isHR())
{{--                <div class="col-sm-6 col-xl-3">--}}
{{--                    <div class="panel panel-tile">--}}
{{--                        <div class="panel-body">--}}
{{--                            <div class="row pv10">--}}
{{--                                <div class="col-xs-5 ph10">--}}
{{--                                    <img src="/assets/img/pages/clipart2.png" class="img-responsive mauto" alt=""/></div>--}}
{{--                                <div class="col-xs-7 pl5">--}}
{{--                                    <h3 class="text-muted text-right"><a href="{{route('employee-manager')}}"> {{ $no_of_employees }}</a></h3>--}}
{{--                                    <h4 class="text-muted text-right">Employees</h4>--}}
{{--                                    --}}{{--<h2 class="fs50 mt5 mbn">385</h2>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-6 col-xl-3">--}}
{{--                    <div class="panel panel-tile">--}}
{{--                        <div class="panel-body">--}}
{{--                            <div class="row pv10">--}}
{{--                                <div class="col-xs-5 ph10"><img src="/assets/img/pages/clipart0.png"--}}
{{--                                                                class="img-responsive mauto" alt=""/></div>--}}
{{--                                <div class="col-xs-7 pl5">--}}
{{--                                    <h3 class="text-muted text-right"><a href="{{route('total-leave-list')}}"> {{ $no_of_leaves }}</a></h3>--}}
{{--                                    <h4 class="text-muted text-right">Leave</h4>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}

            <div class="col-sm-6 col-xl-3">
                <div class="panel panel-tile">
                    <div class="panel-body">
                        <div class="row pv10">
                            <div class="col-xs-5 ph10">
                                <img src="/assets/img/pages/clipart2.png" class="img-responsive mauto" alt=""/></div>
                            <div class="col-xs-7 pl5">
                                <h3 class="text-muted text-center"><a href="{{route('employee-manager')}}"> {{ $no_of_employees }}</a></h3>
                                <h3 class="text-muted text-center"><a href="{{route('employee-manager')}}"> EMPLOYEES [TOTAL]</a></h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xl-3">
                <div class="panel panel-tile">
                    <div class="panel-body">
                        <div class="row pv10">
                            <div class="col-xs-5 ph10"><img src="/assets/img/pages/clipart0.png"
                                                            class="img-responsive mauto" alt=""/></div>
                            <div class="col-xs-7 pl5">
                                <h3 class="text-muted text-center"><a href="{{route('total-leave-list')}}"> {{ $no_of_leaves }}</a></h3>
                                <h3 class="text-muted text-center"> <a href="{{route('total-leave-list')}}"> LEAVES [TOTAL] </a></h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xl-3">
                <div class="panel panel-tile">
                    <div class="panel-body">
                        <div class="row pv10">
                            <div class="col-xs-5 ph10"><img src="/assets/img/pages/clipart5.png"
                                                            class="img-responsive mauto" alt=""/></div>
                            <div class="col-xs-7 pl5">
                                <h3 class="text-muted text-center"><a href="#"> {{ $no_of_present_employee }} / {{ $no_of_employees }}</a></h3>
                                <h3 class="text-muted text-center"><a href="{{route('employee-attendance')}}"> ATTENDANCE [TODAY] </a></h3>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xl-3">
                    <div class="panel panel-tile">
                        <div class="panel-body">
                            <div class="row pv10">
                                <div class="col-xs-5 ph10"><img src="/assets/img/pages/dollar.jpg"
                                                                class="img-responsive mauto" alt=""/></div>
                                <div class="col-xs-7 pl5">
                                    <h3 class="text-muted text-center"><a href="{{route('employee-salary')}}"> $ {{ $total_salary->total_salary }}</a></h3>
                                    <h3 class="text-muted text-center"><a href="{{route('employee-salary')}}"> SALARY [THIS WEEK] </a></h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            @endif
            @if(!Auth::user()->isHR())
            <div class="col-sm-6 col-xl-3">
                <div class="panel panel-tile">
                    <div class="panel-body">
                        <div class="row pv10">
                            <div class="col-xs-5 ph10"><img src="/assets/img/pages/clipart0.png"
                                                            class="img-responsive mauto" alt=""/></div>
                            <div class="col-xs-7 pl5">
                                <h3 class="text-muted"><a href="{{route('my-leave-list')}}"> LEAVES </a></h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xl-3">
                <div class="panel panel-tile">
                    <div class="panel-body">
                        <div class="row pv10">
                            <div class="col-xs-5 ph10"><img src="/assets/img/pages/clipart5.png"
                                                            class="img-responsive mauto" alt=""/></div>
                            <div class="col-xs-7 pl5">
                                <h3 class="text-muted"><a href="{{route('employee-attendance')}}"> Attendance </a></h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif

         </div>
        @if(Auth::user()->isHR())
            <div class="row">
                <div class="col-md-7">
                    <div>
                        <p class="text-center"><b>Total salary distribution of last 4 weeks</b></p>
                        <canvas id="lineChart" height="140"></canvas>
                    </div>
                </div>
                <div class="col-md-5">
                    <div>
                        <p class="text-center"><b>Department</b></p>
                        <canvas id="doughnutChart" height="140"></canvas>
                    </div>
                </div>
            </div>
        @endif
        </div>
         </section>
    @endsection