@extends('hrms.layouts.base')

@section('css')
    @parent()
    <!-- -------------- CSS - allcp forms -------------- -->
    <link rel="stylesheet" type="text/css" href="/assets/allcp/forms/css/forms.css">
@endsection

@section('js')
    @parent()
    {!! Html::script('/assets/allcp/forms/js/jquery-ui-monthpicker.min.js') !!}
    {!! Html::script('/assets/allcp/forms/js/jquery-ui-datepicker.min.js') !!}
    {!! Html::script('/assets/allcp/forms/js/jquery.spectrum.min.js') !!}
    {!! Html::script('/assets/allcp/forms/js/jquery.stepper.min.js') !!}

    <!-- -------------- Plugins -------------- -->
    {!! Html::script('/assets/allcp/forms/js/jquery.validate.min.js') !!}
    {!! Html::script('/assets/allcp/forms/js/jquery.steps.min.js') !!}
    {!! Html::script('/assets/js/custom_form_wizard.js') !!}
    {!!  Html::script ('/assets/js/pages/forms-widgets.js')!!}
    <script src="/assets/js/function.js"></script>
@endsection

@section('content')
    <!-- -------------- Main Wrapper -------------- -->
    <section class="content">

        <!-- -------------- Topbar -------------- -->
        <header id="topbar" class="alt">

            @if(\Route::getFacadeRoot()->current()->uri() == 'edit-emp/{id}')

                <div class="topbar-left">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-icon">
                            <a href="/dashboard">
                                <span class="fa fa-home"></span>
                            </a>
                        </li>
                        <li class="breadcrumb-link">
                            <a href="/dashboard"> Employees </a>
                        </li>
                        <li class="breadcrumb-current-item"> Edit details of {{$emps->name}} </li>
                    </ol>
                </div>

            @else

                <div class="topbar-left">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-icon">
                            <a href="/dashboard">
                                <span class="fa fa-home"></span>
                            </a>
                        </li>
                        <li class="breadcrumb-active">
                            <a href="/dashboard">Dashboard</a>
                        </li>
                        <li class="breadcrumb-link">
                            <a href="/add-employee"> Employees </a>
                        </li>
                        <li class="breadcrumb-current-item"> Add Details</li>
                    </ol>
                </div>

            @endif
        </header>
        <!-- -------------- /Topbar -------------- -->

        <!-- -------------- Content -------------- -->
        <section id="content" class="table-layout animated fadeIn">

            <div class="mw1000 center-block">
                @if(session('message'))
                    {{session('message')}}
                @endif
                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ session::get('flash_message') }}
                    </div>
            @endif

            <!-- -------------- Wizard -------------- -->
                <!-- -------------- Spec Form -------------- -->
                <div class="allcp-form ">

                    <form method="post" action="/" id="custom-form-wizard">
                        {{ csrf_field() }}
                        <div class="wizard steps-bg main-section steps-left">

                            <!-- -------------- step 1 -------------- -->
                            <h4 class="wizard-section-title">
                                <i class="fa fa-user pr5"></i> Personal Details
                            </h4>
                            <section class="wizard-section">

                                <div class="section">
                                    <label for="input002"><h6 class="mb20 mt20">Employee Code</h6></label>
                                    <label for="input002" class="field prepend-icon">
                                        @if(\Route::getFacadeRoot()->current()->uri() == 'edit-emp/{id}')
                                            <input type="text" name="emp_code" id="emp_code" class="gui-input"
                                                   value="@if($emps && $emps->employee->code){{$emps->employee->code}}@endif" required disabled>
                                            <label for="input002" class="field-icon">
                                                <i class="fa fa-barcode"></i>
                                            </label>
                                        @else
                                            <input type="text" name="emp_code" id="emp_code" class="gui-input"
                                                   placeholder="employee code..." value="EMP#{{ $emp_code }}" required disabled>
                                            <label for="input002" class="field-icon">
                                                <i class="fa fa-barcode"></i>
                                            </label>
                                        @endif
                                    </label>
                                </div>

                                <div class="section">
                                    <label for="photo-upload"><h6 class="mb20 mt20"> Photo </h6></label>
                                    <label class="field prepend-icon append-button file">
                                        <span class="button">Choose File</span>
                                        @if(\Route::getFacadeRoot()->current()->uri() == 'edit-emp/{id}')
                                            <input type="hidden" value="edit-emp/{{$emps->id}}" id="url">

                                            <input type="file" class="gui-file" name="photo" id="photo_upload"
                                                   value="@if($emps && $emps->photo){{$emps->photo}}@endif"
                                                   onChange="document.getElementById('uploader1').value = this.value;"
                                                   accept="image/*" >
                                            <input type="text" class="gui-input" id="uploader1"
                                                   placeholder="Select File" accept="image/*" >
                                            <label class="field-icon">
                                                <i class="fa fa-cloud-upload"></i>
                                            </label>
                                        @else
                                            <input type="hidden" value="add-employee" id="url">
                                            <input type="file" class="gui-file" name="photo" id="photo_upload"
                                                   onChange="document.getElementById('uploader1').value = this.value;" accept="image/*" >
                                            <input type="text" class="gui-input" id="uploader1"
                                                   placeholder="Select File" accept="image/*" >
                                            <label class="field-icon">
                                                <i class="fa fa-cloud-upload"></i>
                                            </label>
                                        @endif
                                    </label>
                                </div>

                                <div class="section">
                                    <label for="input002"><h6 class="mb20 mt20">Employee Name </h6></label>
                                    <label for="input002" class="field">
                                        @if(\Route::getFacadeRoot()->current()->uri() == 'edit-emp/{id}')
                                            <input type="text" name="emp_name" id="emp_name" class="gui-input"
                                                   value="@if($emps && $emps->employee->name){{$emps->employee->name}}@endif" required>
                                        @else
                                            <input type="text" name="emp_name" id="emp_name" class="gui-input"
                                                   placeholder="employee name..." required>
                                        @endif
                                    </label>
                                </div>

                                <div class="section">
                                    <label for="input002"><h6 class="mb20 mt20"> Gender </h6></label>
                                    <div class="option-group field">
                                        <label class="field option mb5">
                                            <input type="radio" value="0" name="gender" id="gender"
                                                   @if(isset($emps))@if($emps->employee->gender == '0')checked @endif @endif>
                                            <span class="radio"></span>Male</label>
                                        <label class="field option mb5">
                                            <input type="radio" value="1" name="gender" id="gender"
                                                   @if(isset($emps))@if($emps->employee->gender == '1')checked @endif @endif>
                                            <span class="radio"></span>Female</label>
                                    </div>
                                </div>


                                <div class="section">
                                    <label for="datepicker1" class="field mb5"><h6 class="mb20 mt20">
                                            Date of Birth </h6></label>

                                    <div class="field">
                                        @if(\Route::getFacadeRoot()->current()->uri() == 'edit-emp/{id}')
                                            <input type="text" id="datepicker1" class="gui-input fs13" name="dob"
                                                   value="@if($emps && $emps->employee->date_of_birth){{$emps->employee->date_of_birth}}@endif" required>
                                        @else
                                            <input type="text" id="datepicker1" class="gui-input fs13" name="dob" required>
                                        @endif
                                    </div>
                                </div>

                                <div class="section">
                                    <label for="input002"><h6 class="mb20 mt20"> Mobile Number </h6></label>
                                    <label for="input002" class="field">
                                        @if(\Route::getFacadeRoot()->current()->uri() == 'edit-emp/{id}')
                                            <input type="number" name="mob_number" id="mobile_phone"
                                                   class="gui-input phone-group" maxlength="10" minlength="10" required
                                                   value="@if($emps && $emps->employee->number){{$emps->employee->number}}@endif">
                                        @else
                                            <input type="number" name="mob_number" id="mobile_phone"
                                                   class="gui-input phone-group" maxlength="10" minlength="10" required
                                                   placeholder="mobile number...">
                                        @endif
                                    </label>
                                </div>

                                <div class="section">
                                    <label for="input002"><h6 class="mb20 mt20"> Email </h6></label>
                                    <label for="input002" class="field">
                                        @if(\Route::getFacadeRoot()->current()->uri() == 'edit-emp/{id}')
                                            <input type="email" name="email" id="email"
                                                   class="gui-input phone-group" required
                                                   value="@if($emps && $emps->employee->email){{$emps->employee->email}}@endif">
                                        @else
                                            <input type="email" name="email" id="email"
                                                   class="gui-input phone-group" required
                                                   placeholder="mobile number...">
                                        @endif
                                    </label>
                                </div>



                                <div class="section">
                                    <label for="input002"><h6 class="mb20 mt20"> Address </h6></label>
                                    <label for="input002" class="field">
                                        @if(\Route::getFacadeRoot()->current()->uri() == 'edit-emp/{id}')
                                            <input type="text" name="address" id="address" class="gui-input"
                                                   value="@if($emps && $emps->employee->address){{$emps->employee->address}}@endif" required>
                                        @else
                                            <input type="text" placeholder="Full address..." name="address"
                                                   id="address" class="gui-input" required>
                                        @endif
                                    </label>
                                </div>
                                <!-- -------------- /section -------------- -->
                            </section>

                            <!-- -------------- step 2 -------------- -->
                            <h4 class="wizard-section-title">
                                <i class="fa fa-user-secret pr5"></i> Employment details
                            </h4>
                            <section class="wizard-section">
                                <div class="section">
                                    <label for="datepicker4" class="field prepend-icon mb5"><h6 class="mb20 mt20">
                                            Date of Joining </h6></label>

                                    <div class="field prepend-icon">
                                        @if(\Route::getFacadeRoot()->current()->uri() == 'edit-emp/{id}')
                                            <input type="text" id="datepicker4" class="gui-input fs13" name="doj"
                                                   value="@if($emps && $emps->employee->date_of_joining){{$emps->employee->date_of_joining}}@endif" required>
                                            <label class="field-icon">
                                                <i class="fa fa-calendar"></i>
                                            </label>
                                        @else
                                            <input type="text" id="datepicker4" class="gui-input fs13" name="doj" required>
                                            <label class="field-icon">
                                                <i class="fa fa-calendar"></i>
                                            </label>
                                        @endif
                                    </div>
                                </div>

                                <div class="section">
                                    <label for="input002"><h6 class="mb20 mt20"> Role </h6></label>
                                    @if(\Route::getFacadeRoot()->current()->uri() == 'edit-emp/{id}')
                                        <select class="select2-single form-control" name="role" id="role" readonly required>
                                            <option value="">Select role</option>
                                            @foreach($roles as $role)
                                                @if($emps->role->role->id == $role->id)
                                                    <option value="{{$role->id}}" selected>{{$role->name}}</option>
                                                @endif
                                                <option value="{{$role->id}}">{{$role->name}}</option>
                                            @endforeach
                                        </select>
                                    @else
                                        <select class="select2-single form-control" name="role" id="role" required>
                                            <option value="">Select role</option>
                                            @foreach($roles as $role)
                                                <option value="{{$role->id}}">{{$role->name}}</option>
                                            @endforeach
                                        </select>
                                    @endif
                                </div>

                                <div class="section">
                                    <label for="input002"><h6 class="mb20 mt20"> Department </h6></label>
                                    <select class="select2-single form-control" name="department" id="department" required>
                                        <option value="">Select department</option>
                                        @if(\Route::getFacadeRoot()->current()->uri() == 'edit-emp/{id}')
                                            @if($emps->employee->department == 'Marketplace')
                                                <option value="Marketplace" selected>Marketplace</option>
                                                <option value="Social Media">Social Media</option>
                                                <option value="IT">IT</option>
                                            @elseif($emps->employee->department == 'Social Media')
                                                <option value="Marketplace">Marketplace</option>
                                                <option value="Social Media" selected>Social Media</option>
                                                <option value="IT">IT</option>
                                            @else
                                                <option value="Marketplace">Marketplace</option>
                                                <option value="Social Media">Social Media</option>
                                                <option value="IT" selected>IT</option>
                                            @endif
                                        @else
                                            <option value="Marketplace">Marketplace</option>
                                            <option value="Social Media">Social Media</option>
                                            <option value="IT">IT</option>
                                        @endif
                                    </select>
                                </div>

                                <div class="section">
                                    <label for="input002"><h6 class="mb20 mt20"> Rate per hour </h6>
                                    </label>
                                    <label for="input002" class="field prepend-icon">
                                        @if(\Route::getFacadeRoot()->current()->uri() == 'edit-emp/{id}')
                                            <input type="number" name="salary" id="salary" class="gui-input"
                                                   value="@if($emps && $emps->employee->salary){{$emps->employee->salary}}@endif" readonly>
                                            <label for="input002" class="field-icon">
                                                <i class="fa fa-dollar"></i>
                                            </label>
                                        @else
                                            <input type="number" placeholder="e.g 12000" name="salary"
                                                   id="salary" class="gui-input">
                                            <label for="input002" class="field-icon">
                                                <i class="fa fa-dollar"></i>
                                            </label>
                                        @endif
                                    </label>
                                </div>

                                <div class="section">
                                    <label for="input002"><h6 class="mb20 mt20"> TFN Number </h6></label>
                                    <label for="input002" class="field prepend-icon">
                                        @if(\Route::getFacadeRoot()->current()->uri() == 'edit-emp/{id}')
                                            <input type="text" name="tfn_number" id="tfn_number" class="gui-input"
                                                   value="@if($emps && $emps->employee->tfn_number){{$emps->employee->tfn_number}}@endif">
                                        @else
                                            <input type="text" placeholder="TFN" name="tfn_number"
                                                   id="pan_number" class="gui-input">

                                        @endif
                                    </label>
                                </div>


                            </section>

                            <!-- -------------- step 3 -------------- -->
                            <h4 class="wizard-section-title">
                                <i class="fa fa-file-text pr5"></i> Banking Details
                            </h4>
                            <section class="wizard-section">
                                <!-- -------------- /section -------------- -->
                                <div class="section">
                                    <label for="input002"><h6 class="mb20 mt20"> Bank Name </h6></label>
                                    <label for="input002" class="field prepend-icon">
                                        @if(\Route::getFacadeRoot()->current()->uri() == 'edit-emp/{id}')
                                            <input type="text" name="bank_name" id="bank_name" class="gui-input"
                                                   value="@if($emps && $emps->employee->bank_name){{$emps->employee->bank_name}}@endif">
                                            <label for="input002" class="field-icon">
                                                <i class="fa fa-columns"></i>
                                            </label>
                                        @else
                                            <input type="text" placeholder="name of bank..." name="bank_name"
                                                   id="bank_name" class="gui-input">
                                            <label for="input002" class="field-icon">
                                                <i class="fa fa-columns"></i>
                                            </label>
                                        @endif
                                    </label>
                                </div>

                                <div class="section">
                                    <label for="input002"><h6 class="mb20 mt20"> BSB </h6></label>
                                    <label for="input002" class="field prepend-icon">
                                        @if(\Route::getFacadeRoot()->current()->uri() == 'edit-emp/{id}')
                                            <input type="text" name="bsb" id="bsb" class="gui-input"
                                                   value="@if($emps && $emps->employee->bsb){{$emps->employee->bsb}}@endif">
                                            <label for="input002" class="field-icon">
                                                <i class="fa fa-font"></i>
                                            </label>
                                        @else
                                            <input type="text" placeholder="BSB..." name="ifsc_code"
                                                   id="ifsc_code" class="gui-input">
                                            <label for="input002" class="field-icon">
                                                <i class="fa fa-font"></i>
                                            </label>
                                        @endif
                                    </label>
                                </div>

                                <div class="section">
                                    <label for="input002"><h6 class="mb20 mt20"> Bank Account Number </h6></label>
                                    <label for="input002" class="field prepend-icon">
                                        @if(\Route::getFacadeRoot()->current()->uri() == 'edit-emp/{id}')
                                            <input type="text" name="account_number" id="bank_account_number"
                                                   class="gui-input"
                                                   value="@if($emps && $emps->employee->account_number){{$emps->employee->account_number}}@endif">
                                            <label for="input002" class="field-icon">
                                                <i class="fa fa-list"></i>
                                            </label>
                                        @else
                                            <input type="text" placeholder="Bank account number"
                                                   name="account_number" id="bank_account_number" class="gui-input">
                                            <label for="input002" class="field-icon">
                                                <i class="fa fa-list"></i>
                                            </label>
                                        @endif
                                    </label>
                                </div>

                            </section>

                        </div>
                        <!-- -------------- /Wizard -------------- -->

                    </form>
                    <!-- -------------- /Form -------------- -->

                </div>
                <!-- -------------- /Spec Form -------------- -->

            </div>

        </section>
        <!-- -------------- /Content -------------- -->

    </section>

    <!-- Notification modal -->

    <div class="modal fade" tabindex="-1" role="dialog" id="notification-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div id="modal-header" class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <p></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection