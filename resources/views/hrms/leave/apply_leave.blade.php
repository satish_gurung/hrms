@extends('hrms.layouts.base')

@section('css')
    @parent()
    <link href='{{ asset('assets/fullcalendar/packages/core/main.css') }}' rel='stylesheet' />
    <link href='{{ asset('assets/fullcalendar/packages/daygrid/main.css') }}' rel='stylesheet' />
    <link href='{{ asset('assets/fullcalendar/packages/timegrid/main.css') }}' rel='stylesheet' />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
@endsection

@section('js')
    @parent()
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src='{{ asset('assets/fullcalendar/packages/core/main.js') }}'></script>
    <script src='{{ asset('assets/fullcalendar/packages/interaction/main.js') }}'></script>
    <script src='{{ asset('assets/fullcalendar/packages/daygrid/main.js') }}'></script>
    <script src='{{ asset('assets/fullcalendar/packages/timegrid/main.js') }}'></script>
    <script src="{{ asset('assets/js/function.js') }}"></script>
    <script>
        let url;
        let role = 'emp';
        @if(Auth::user()->isHR())
            url = '/api/getAllLeaveRequest';
            role = 'hr';
        @else
                url = '/getLeaveRequestByUser';
                role = 'emp';
        @endif
        document.addEventListener('DOMContentLoaded', function() {
            loadCalendar();
            function loadCalendar(){
                var calendarEl = document.getElementById('calendar');

                var calendar = new FullCalendar.Calendar(calendarEl, {
                    plugins: [ 'interaction', 'dayGrid', 'timeGrid' ],
                    defaultView: 'dayGridMonth',
                    defaultDate: moment().format('YYYY-MM-DD'),
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'dayGridMonth,timeGridWeek,timeGridDay'
                    },
                    navLinks: true, // can click day/week names to navigate views
                    selectable: true,
                    selectMirror: true,
                    select: function(arg) {
                        var check = moment(arg.start).format('YYYY-MM-DD');
                        var today = moment().format('YYYY-MM-DD');
                        if(check < today)
                        {
                            // Previous Day. show message if you want otherwise do nothing.
                            // So it will be unselectable
                            alert("Please select the future dates only");
                        }
                        else
                        {
                            console.log(arg);
                            // Its a right date
                            // Do something
                            $('#apply-modal-leave').dialog({ title : "Apply for leave"});
                            $('#dateFrom').val(moment(arg.start).format('YYYY-MM-DD'));
                            let to_date = moment(arg.end).subtract(1, "days").format('YYYY-MM-DD');
                            $('#dateTo').val(to_date);
                            getTotalNoOfLeaves();
                        }

                    },
                    eventClick: function(calEvent, jsEvent, view) {
                        console.log(calEvent);
                        console.log(calEvent.event);
                        console.log(view);
                        // $('#apply-modal-leave').dialog({ title : "Apply for leave"});
                        // $('#dateFrom').val(moment(arg.start).format('YYYY-MM-DD'));
                        // $('#dateTo').val(moment(arg.end).format('YYYY-MM-DD'));
                        // alert('Event: ' + calEvent.event.title);
                        if(role === 'hr' && calEvent.event.backgroundColor === 'blue'){
                            $('#leave-ap-rj-modal').modal('show');
                            $('#leave-ap-rj-title').text(calEvent.event.title);
                            $('#leave_id').val(calEvent.event.id);
                        } else if(role === 'emp' && calEvent.event.backgroundColor === 'blue'){
                            $('#leave-delete-modal').modal('show');
                            $('#leave-delete-title').text(calEvent.event.title);
                            $('#leave_delete_id').val(calEvent.event.id);
                        }else {
                            alert('Event: ' + calEvent.event.title);
                        }
                        // alert('View: ' + view.name);


                        // change the border color just for fun
                        $(this).css('border-color', 'red');

                    },
                    editable: true,
                    eventLimit: true, // allow "more" link when too many events
                    events: url
                });

                calendar.render();
            }
        });
    </script>
@endsection

@section('content')
        <!-- START CONTENT -->
<div class="content">

    <header id="topbar" class="alt">
        <div class="topbar-left">
            <ol class="breadcrumb">
                <li class="breadcrumb-icon">
                    <a href="/dashboard">
                        <span class="fa fa-home"></span>
                    </a>
                </li>
                <li class="breadcrumb-active">
                    <a href="/dashboard"> Dashboard </a>
                </li>
                <li class="breadcrumb-link">
                    <a href=""> Leave </a>
                </li>
                <li class="breadcrumb-current-item"> Apply Leave</li>
            </ol>
        </div>
    </header>
    <!-- -------------- Content -------------- -->
    <section id="content" class="table-layout animated fadeIn">
        <!-- -------------- Column Center -------------- -->
        <div class="chute-affix" data-spy="affix" data-offset-top="200">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-success">
                    <div class="panel">
                        <div class="panel-heading">
                            @if(Session::has('success'))
                                <div class="alert alert-danger">
                                    {{ Session::get('success') }}
                                </div>
                            @endif
                            <div>
                                <span class="panel-title hidden-xs pull-left"> Request Leave</span>
                                @if(Auth::user()->isHR())
                                    <a href="{{ url('/total-leave-list') }}" class="btn btn-primary pull-right"><i class="fa fa-list"></i> View in List</a>
                                @else
                                    <a href="{{ url('/my-leave-list') }}" class="btn btn-primary pull-right"><i class="fa fa-list"></i> View in List</a>
                                @endif

                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="panel-body pn" style="padding: 50px">
                            <div id='calendar'></div>

                            <div id="apply-modal-leave" style="display: none;">
                                <div class="text-center" id="show-leave-count"></div>
                                <div class="table-responsive">
                                    <div class="panel-body p25 pb10">
                                        @if(session('message'))
                                            {{session('message')}}
                                        @endif
                                        @if(Session::has('flash_message'))
                                            <div class="alert alert-success">
                                                {{ session::get('flash_message') }}
                                            </div>
                                        @endif
                                        {!! Form::open(['class' => 'form-horizontal', 'method' => 'post']) !!}

                                        <div class="form-group">
                                            <label class="col-md-2 control-label"> Leave Type </label>
                                            <div class="col-md-10">
                                                <input type="hidden" value="{!! csrf_token() !!}" id="token">
                                                <input type="hidden" value="{{\Auth::user()->id}}" id="user_id">
                                                <select class="select2-multiple form-control select-primary leave_type"
                                                        name="leave_type" required>
                                                    <option value="" selected>Select One</option>
                                                    @foreach($leaves as $leave)
                                                        <option value="{{$leave->id}}">{{$leave->leave_type}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label for="date_from" class="col-md-2 control-label"> Date From </label>
                                            <div class="col-md-10">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar text-alert pr10"></i>
                                                    </div>
                                                    <input type="text" id="dateFrom" class="select2-single form-control"
                                                           name="dateFrom" required>
                                                </div>
                                            </div>

                                        </div>

                                            <div class="form-group">
                                                <label for="date_to" class="col-md-2 control-label"> Date To </label>
                                                <div class="col-md-10">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar text-alert pr10"></i>
                                                        </div>
                                                        <input type="text" id="dateTo" class="select2-single form-control"
                                                               name="dateTo" required>
                                                    </div>

                                                </div>
                                            </div>


                                        <div class="form-group">
                                            <label for="time_from" class=" col-md-2 control-label  "> Time From </label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="imoon imoon-clock text-alert pr10"></i>
                                                    </div>
                                                    <input type="text" id="timepicker1" class="select2-single form-control" value="9:30"
                                                           name="time_from" required>
                                                </div>
                                            </div>
                                            <label for="time_to" class="col-md-2 control-label"> Time To </label>
                                            <div class="col-md-3">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="imoon imoon-clock text-alert pr10"></i>
                                                    </div>
                                                    <input type="text" id="timepicker4" class="select2-single form-control" value="18:00"
                                                           name="time_to" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="input002" class="col-md-2 control-label"> Days </label>
                                            <div class="col-md-10">
                                                <input id="total_days" name="number_of_days" value="" readonly="readonly"
                                                       type="text" size="90" class="select2-single form-control"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="input002" class="col-md-2 control-label"> Reason </label>
                                            <div class="col-md-10">
                                                <input type="text" id="textarea1" class="select2-single form-control"
                                                       name="reason" required>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-md-2 control-label"></label>

                                            <div class="col-md-4">

                                                <input type="submit" class="btn btn-bordered btn-info btn-block"
                                                       value="Submit">
                                            </div>
                                            <div class="col-md-4"><a href="/apply-leave" >
                                                    <input type="button" class="btn btn-bordered btn-success btn-block" value="Reset"></a></div>

                                        </div>

                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </section>

    <div class="modal fade" tabindex="-1" role="dialog" id="leave-ap-rj-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div id="modal-header" class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="leave-ap-rj-title"></h4>
                </div>
                <div class="modal-body">
                    <textarea id="remark-text" class="form-control" placeholder="Remarks"></textarea>
                    <input type="hidden" id="leave_id">

                    <div id="loader" class="hidden text-center">
                        <img src="/photos/76.gif" />
                    </div>
                    <div id="status-message" class="hidden">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="approve-button">Approve</button>
                    <button type="button" class="btn btn-danger" id="reject-button">Reject</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade" tabindex="-1" role="dialog" id="leave-delete-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div id="modal-header" class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="leave-delete-title"></h4>
                </div>
                <div class="modal-body">

                    <form action="{{ url('/delete-leave-request') }}" method="post">
                        {{ csrf_field() }}
                        <input name="leave_id" type="hidden" id="leave_delete_id">
                        <button type="submit" class="btn btn-danger" id="reject-button">Delete</button>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

</div>
</div>
    @push('scripts')
        <script src="/assets/js/custom.js"></script>
        <script src="/assets/js/function.js"></script>
    @endpush
@endsection