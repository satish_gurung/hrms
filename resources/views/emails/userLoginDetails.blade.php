@component('mail::message')
# Login Details

Email: {{ $user->email }}<br>
Password: 123456<br>
Please change your password as soon as possible by logging in into your Account.<br>

@component('mail::button', ['url' => config('app.url')])
    Login
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent