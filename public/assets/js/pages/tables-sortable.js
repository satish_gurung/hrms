'use strict';
//  Author: HRMS.com
//  tables-sortable.html scripts
//

(function($) {

    $(document).ready(function() {

        "use strict";

        // Init Demo JS
        Demo.init();


        // Init Theme Core
        Core.init();

    });

})(jQuery);
