'use strict';

(function ($) {

    $(document).ready(function () {

        "use strict";


        // Form Wizard
        var form = $("#custom-form-wizard");
        form.validate({
            errorPlacement: function errorPlacement(error, element) {
                element.after(error);
            },
            rules: {
                confirm: {
                    equalTo: "#password"
                }
            }
        });
        form.children(".wizard").steps({
            headerTag: ".wizard-section-title",
            bodyTag: ".wizard-section",
            onStepChanging: function (event, currentIndex, newIndex) {
                form.validate().settings.ignore = ":disabled,:hidden";
                return form.valid();
            },
            onFinishing: function (event, currentIndex) {
                form.validate().settings.ignore = ":disabled";
                return form.valid();
            },
            onFinished: function (event, currentIndex) {
                event.preventDefault();
                var emp_name = $('#emp_name').val();
                //alert(emp_name)
                var emp_code = $('#emp_code').val();
                var emp_status = $("input[name='emp_status']:checked").val();
                var role = $('#role').val();
                var gender = $("input[name='gender']:checked").val();
                var datepicker1 = $('#datepicker1').val();
                var datepicker4 = $('#datepicker4').val();
                var mobile_phone = $('#mobile_phone').val();
                var address = $('#address').val();
                var email = $('#email').val();
                var department = $('#department').val();
                var salary = $('#salary').val();
                var bank_account_number = $('#bank_account_number').val();
                var bank_name = $('#bank_name').val();
                var bsb = $('#bsb').val();
                var tfn_number = $('#tfn_number').val();
                var token = $("input[name='_token']").val();


                var photo = document.getElementById('photo_upload');
                var formData = new FormData();

                if (photo.value != '') {
                    formData.append('photo', photo.files[0], photo.value);
                }
                formData.append('emp_name', emp_name);
                formData.append('emp_code', emp_code);
                formData.append('emp_status', emp_status);
                formData.append('role', role);
                formData.append('gender', gender);
                formData.append('date_of_birth', datepicker1);
                formData.append('date_of_joining', datepicker4);
                formData.append('number', mobile_phone);
                formData.append('address', address);
                formData.append('department', department);
                formData.append('salary', salary);
                formData.append('account_number', bank_account_number);
                formData.append('bank_name', bank_name);
                formData.append('bsb', bsb);
                formData.append('tfn_number', tfn_number);
                formData.append('email', email);
                formData.append('_token', token);


                var url = $('#url').val();
                $.ajax({
                    type: 'POST',
                    url: '/' + url,
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        window.location = "/employee-manager";
                        // $('#modal-header').attr('class', 'modal-header ' + parsed.class);

                    },
                    error: function (error) {
                        $('.modal-title').text(error.responseJSON.title);
                        $('.modal-body').text(error.responseJSON.message);
                        $('#notification-modal').modal('show');
                    }
                });

            }
        });

        // Init Wizard
        var formWizard = $('.wizard');
        var formSteps = formWizard.find('.steps');

        $('.wizard-options .holder-style').on('click', function (e) {
            e.preventDefault();

            var stepStyle = $(this).data('steps-style');

            var stepRight = $('.holder-style[data-steps-style="steps-right"]');
            var stepLeft = $('.holder-style[data-steps-style="steps-left"]');
            var stepJustified = $('.holder-style[data-steps-style="steps-justified"]');

            if (stepStyle === "steps-left") {
                stepRight.removeClass('holder-active');
                stepJustified.removeClass('holder-active');
                formWizard.removeClass('steps-right steps-justified');
            }
            if (stepStyle === "steps-right") {
                stepLeft.removeClass('holder-active');
                stepJustified.removeClass('holder-active');
                formWizard.removeClass('steps-left steps-justified');
            }
            if (stepStyle === "steps-justified") {
                stepLeft.removeClass('holder-active');
                stepRight.removeClass('holder-active');
                formWizard.removeClass('steps-left steps-right');
            }

            if ($(this).hasClass('holder-active')) {
                formWizard.removeClass(stepStyle);
            } else {
                formWizard.addClass(stepStyle);
            }

            $(this).toggleClass('holder-active');
        });
    });

})(jQuery);
