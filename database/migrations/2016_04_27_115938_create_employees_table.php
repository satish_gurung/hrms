<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('photo')->nullable();
            $table->string('code')->unique();
            $table->string('name');
            $table->tinyInteger('gender');
            $table->date('date_of_birth');
            $table->date('date_of_joining');
            $table->string('email');
            $table->string('number');
            $table->string('address');
            $table->string('department');
            $table->double('salary');
            $table->string('tfn_number')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('bsb')->nullable();
            $table->string('account_number')->nullable();
            $table->boolean('status')->default(1);
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employees');
    }
}