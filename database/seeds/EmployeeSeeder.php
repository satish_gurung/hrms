<?php

use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('employees')->insert([
            'photo' => '',
            'code' => 'EMP#1',
            'status' => '1',
            'name' => 'HR Manager',
            'gender' => '1',
            'date_of_birth' => '',
            'date_of_joining' => '',
            'number' => '9999999999',
            'email' => 'test@demo.com',
            'address' => '',
            'department' => 'IT',
            'salary' => '40',
            'account_number' => '',
            'bank_name' => '',
            'bsb' => '',
            'tfn_number' => '',
            'user_id' => '1'
        ]);
    }
}
